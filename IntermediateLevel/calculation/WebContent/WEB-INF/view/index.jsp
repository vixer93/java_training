<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>計算機</title>
  <meta charset="UTF-8">
</head>
<body>
  <h1>計算機</h1>
  <form action="./Calculate" method="post">
    <label for="first-num">第1の数：</label>
    <input id="first-num" type="text" name="firstNum"><br>
    <label for="operator">演算子</label>
    <select name="operator">
      <option label="+" value="1">+</option>
      <option label="-" value="2">-</option>
      <option label="×" value="3">×</option>
      <option label="÷" value="4">÷</option>
    </select><br>
    <label for="second-num">第2の数：</label>
    <input id="second-num" type="text" name="secondNum"><br>
    <input type="submit" value="計算">
  </form>
  <% Double  answer     = (Double)request.getAttribute("answer"); %>
  <% Boolean has_answer = (Boolean)request.getAttribute("has_answer"); %>
  <% if (has_answer){ %>
  	<%= answer %>
  <%}else{ %>
  	 解なし
  <%}%>
</body>
</html>