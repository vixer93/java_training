package calculate.controller;

public class Calculate {
    int    operator;
    double first_num;
    double second_num;

    public Calculate(int operator, double first_num, double second_num) {
    	this.operator   = operator;
    	this.first_num  = first_num;
    	this.second_num = second_num;
    }

    public boolean HasAnswer() {
    	if(this.operator == 4 && this.second_num == 0) {
    		return false;
    	}else {
    		return true;
    	}
    }

    public double Calculation() {
    	double answer = 0.0;

	    switch(this.operator) {
	    case 1:
	    	answer = this.first_num + this.second_num;
	    	return answer;
	    case 2:
	    	answer = this.first_num - this.second_num;
	    	return answer;
	    case 3:
	    	answer = this.first_num * this.second_num;
	    	return answer;
	    case 4:
	    	answer = this.first_num / this.second_num;
	    	return answer;
	    default:
	    	return answer;
	    }
    }
}