package calculate.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Calculate")
public class CalculateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public CalculateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String view = "/WEB-INF/view/index.jsp";
	    RequestDispatcher dispatcher = request.getRequestDispatcher(view);
	    dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");

		Calculate formula = new Calculate(
											Integer.parseInt(request.getParameter("operator")),
											Double.parseDouble(request.getParameter("firstNum")),
											Double.parseDouble(request.getParameter("secondNum"))
									     );

		request.setAttribute("answer", formula.Calculation());
		request.setAttribute("has_answer", formula.HasAnswer());

		String view = "/WEB-INF/view/index.jsp";
	    RequestDispatcher dispatcher = request.getRequestDispatcher(view);
	    dispatcher.forward(request, response);
	}

}