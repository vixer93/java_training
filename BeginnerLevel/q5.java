import java.util.*;

public class Main {
  public static void Testjudge(int test){
    if (test >= 80 && test <= 100){
      System.out.println("優");
    }else if (test >= 70 && test <80){
      System.out.println("良");
    }else if (test >= 60 && test < 70){
      System.out.println("可");
    }else if (test >= 0 && test <60){
      System.out.println("不可");
    }else{
      System.out.println("不正な値です");
    }
  }

  public static void main(String[] args) throws Exception {
    System.out.println("点数を入力してください");
    Scanner scan = new Scanner(System.in);
    int test = Integer.parseInt(scan.next());
    Testjudge(test);
  }
}
