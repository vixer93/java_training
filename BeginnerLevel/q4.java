import java.util.*;

public class Main {
  public static double calculation(double x){
    x += 7;
    x *= 3;
    x /= 2;
    return x;
  }

  public static void main(String[] args) throws Exception {
    double x = 3;
    System.out.println(calculation(x));
  }
}
