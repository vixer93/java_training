import java.util.*;

public class Main {

  public static double average(List<Integer> num_array){
    int total_num = 0;
    int i = 0;

    for(int num : num_array){
      total_num += num;
      i += 1;
    }
    return total_num / i;
  }

  public static void main(String[] args) throws Exception {
    List<Integer> num_array = Arrays.asList(0,1,2,3,4);
    System.out.println(average(num_array));
  }
}
