import java.util.*;

public class Main {

  public static void SearchHash(ArrayList<ArrayList<String>> multi_array){
    for(ArrayList<String> array : multi_array){
      if(array.contains("###")){
        System.out.println("###");
      }
    }
  }

  public static void main(String[] args) throws Exception {
    ArrayList<ArrayList<String>> multi_list = new ArrayList<ArrayList<String>>();

    ArrayList<String> list1 = new ArrayList<String>();
    list1.add("###");
    list1.add("a");
    list1.add("b");

    ArrayList<String> list2 = new ArrayList<String>();
    list2.add("c");
    list2.add("###");

    ArrayList<String> list3 = new ArrayList<String>();
    list3.add("###");
    list3.add("d");

    multi_list.add(list1);
    multi_list.add(list2);
    multi_list.add(list3);

    SearchHash(multi_list);
  }
}
