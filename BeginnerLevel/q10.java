import java.util.*;

public class Main {

  public static List<Integer> TwiceVal(List<Integer> num_array){
    List<Integer> num_twice = new ArrayList<Integer>();
    int i =0;

    for(int num : num_array){
      num_twice.add(num*2);
      i += 1;
    }
    return num_twice;
  }

  public static void main(String[] args) throws Exception {
    List<Integer> num_array = Arrays.asList(0,1,2,3,4);
    System.out.println(TwiceVal(num_array));
  }
}
