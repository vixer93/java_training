import java.util.*;

public class Main {
  public static void SayHello(){
    System.out.print("Hello");
  }

  public static void SayRoto(){
    System.out.print("Roto");
  }

  public static void SayWorks(){
    System.out.print("Works");
  }

  public static void main(String[] args) throws Exception {
    SayHello();
    SayRoto();
    SayWorks();
  }
}
