import java.util.*;

public class Main {

  //最大値、最小値を配列として返す関数
  public static List<Integer> JudgeMaxMin_Array(List<Integer> num_array){
    List<Integer> max_min = new ArrayList<Integer>();
    int max_num = num_array.get(0);
    int min_num = num_array.get(0);

    //最大、最小の計算
    for(int i = 1; i < num_array.size(); i++){
      if(max_num < num_array.get(i)){
        max_num = num_array.get(i);
      }else if (min_num > num_array.get(i)){
        min_num = num_array.get(i);
      }
    }

    max_min.add(max_num);
    max_min.add(min_num);
    return max_min;
  }

  //最大値、最小値を連想配列で返す関数
  public static HashMap<String, Integer> JudgeMaxMin_AssoArray(List<Integer> num_array){
    HashMap<String, Integer> max_min = new HashMap<String, Integer>();
    int max_num = num_array.get(0);
    int min_num = num_array.get(0);

    //最大、最小の計算
    for(int i = 1; i < num_array.size(); i++){
      if(max_num < num_array.get(i)){
        max_num = num_array.get(i);
      }else if (min_num > num_array.get(0)){
        min_num = num_array.get(i);
      }
    }

    max_min.put("最大値",max_num);
    max_min.put("最小値",min_num);
    return max_min;
  }

  public static void main(String[] args) throws Exception {
    List<Integer> num_array = Arrays.asList(1,2,3,4,5);

    //最大、最小値の入った配列の表示処理
    System.out.println(JudgeMaxMin_Array(num_array));

    //最大、最小値の入った連想配列の表示処理
    HashMap<String, Integer> max_min = JudgeMaxMin_AssoArray(num_array);
    System.out.println("最大値："+max_min.get("最大値"));
    System.out.println("最小値："+max_min.get("最小値"));
  }
}
