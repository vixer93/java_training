import java.util.*;
//Arrays.asList().contains()使用のためには以下二つのimportが必要
//今回はjava.util.*;で全てimport済み
// import java.util.Arrays;
// import java.util.List;

public class Main {
  public static void DisplayDays(int month){
    //int[]ではArrays.asListが動作しなかったためInteger[]で定義
    List<Integer> thirty_one_days = Arrays.asList(1,3,5,7,8,10,12);
    List<Integer> thirty_days     = Arrays.asList(4,6,9,11);

    if (thirty_one_days.contains(month)){
      System.out.println("31日です");
    }else if (thirty_days.contains(month)){
      System.out.println("30日です");
    }else if (month == 2){
      System.out.println("28日です");
    }else{
      System.out.println("1~12の月を入力してください");
    }
  }

  public static void main(String[] args) throws Exception {
    System.out.println("日にちを知りたい月の数字を入力してください");
    Scanner scan = new Scanner(System.in);
    int month = Integer.parseInt(scan.next());
    DisplayDays(month);
  }
}
